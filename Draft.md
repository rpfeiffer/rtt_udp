# UDP Round-Trip-Time Measurement

This is a showcase for socket programming with GNU/Linux and C/C++.

## Requirements

The project has two build files, one for GNU make and one for the Ninja build system. The
build.ninja file is the main build file. The following libraries must be present for
compiling the code:

- Boost Asio
- Boost Program Options
- Libuuid (uuid-dev)
- TBB library for C++20 parallel algorithms

To compile this code the following Debian packages are needed:

- clang-15 (from the Clang Debian repository)
- libboost-program-options1.74-dev (from Debian backports)
- libboost-system1.74-dev (from Debian backports)
- libtbb-dev
- lld-15 (from the Clang Debian repository)
- uuid-dev

If your Boost libraries are older and you get a warning about uint64_t data types, then
you need to upgrade to a more recent version of Boost (Debian 11 with backports will do
nicely).

## Example implementation

The basic use of UDP sockets was inspired from this article:
https://www.geeksforgeeks.org/udp-server-client-implementation-c/

## Purpose

Start UDP server, wait for connections.
Client sends message, server answers.

## Message Format

Ping message:
UUID | Timestamp | FQDN (optionally encrypted, not implemented)

Pong message:
UUID | Timestamp | FQDN (optionally encrypted, not implemented)

## RTT

Client calculates RTT by measuring timestamp of sent packet and measuring
the answer from the server. In-packet timestamps are also substracted. This is not
the most accurate way of measuring time, but it will do for the prototype.

## Logged data

Client should save all packet metadata and create a statistic. Logging is implemented
(simple log into a CSV file).
