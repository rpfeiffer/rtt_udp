// =====================================================================================
//
//       Filename:  udprtt.cc
//
//    Description:  UDP RTT measurement tool.
//
//        Version:  1.0
//        Created:  11. des. 2022 kl. 01.49 +0100
//       Revision:  none
//       Compiler:  clang
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web-luchs.at/
//
// =====================================================================================

#include <chrono>
#include <clocale>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <locale>
#include <random>
#include <string>

// C
#include <sysexits.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

// Our includes
#include "configuration.h"
#include "networking.h"

using namespace std;
namespace fs = std::filesystem;

//----------------------------------------------------------------------
// Definitions
//----------------------------------------------------------------------

const unsigned short int version_major = 0;
const unsigned short int version_minor = 39;

const string programme_name("udprtt");

//----------------------------------------------------------------------
// Globals     
//----------------------------------------------------------------------

class config_options options;
int signal_received = 0;

void signal_handler( int signal ) {
#ifdef DEBUG
    cout << "DEBUG: Received signal " << signal << "." << endl;
#endif
    signal_received++;
    return;
}

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------
// The cyber pathogen starts here!

int main( int argc, char **argv ) {
    // Return code (definitions taken from sysexits.h)
    int rc = EX_OK;

    // --- Parse configuration options --------------------------------
    options.version_major  = version_major;
    options.version_minor  = version_minor;
    options.programme_name = programme_name;

    rc = options.parse_configuration( argc, argv );
    if ( options.help_was_called ) {
        return(rc);
    }

    // --- Run as a server --------------------------------------------
    if ( options.is_server ) {
        if ( not options.opt_quiet ) {
            cout << "Binding server to address <" << options.conf_address << "> and port " << options.conf_port << "." << endl;
        }
        rtt_server server( options.conf_address, options.opt_port );
        if ( server.is_initialised ) {
            // Redirect signals, so that we end with calling all destructors
            signal( SIGHUP,  signal_handler );
            signal( SIGINT,  signal_handler );
            signal( SIGPWR,  signal_handler );
            signal( SIGQUIT, signal_handler );
            signal( SIGTERM, signal_handler );
            signal( SIGUSR1, signal_handler );
            signal( SIGUSR2, signal_handler );
            // Start server event loop
            server.event_loop();
        }
        else {
            cerr << "ERROR: Server could not be initialised. Reason: " << server.last_error << endl;
        }
    }

    // --- Run as a client --------------------------------------------
    if ( options.is_client ) {
        rtt_client client( options.opt_target, options.opt_port );
        delta_ts rtt_data;

        if ( not options.conf_data_file.empty() ) {
            if ( not rtt_data.set_data_file_name( options.conf_data_file ) ) {
                cerr << "ERROR: Could not set data log file name." << endl;
            }
        }

        if ( options.conf_checkpoint_set ) {
            rtt_data.set_save_checkpoint( options.conf_checkpoint_set );
        }

        if ( client.is_initialised ) {
            unsigned int i = 1;
            unsigned short width = to_string( options.opt_count ).length();
            uint64_t t;
            double ts;
            double ms;
            string msg;

            do {
                if ( client.create_ping_packet() ) {
                    if ( client.send_packet() ) {
                        if ( client.recv_packet() ) {
                            msg = client.get_packet_data_as_string();
                            if ( not msg.empty() ) {
                                t = client.get_delta_micro();
                                ms = client.get_delta_milli();
                                ts = client.get_delta_seconds();
                                rtt_data.add_data_point( t );
                                if ( not options.opt_quiet ) {
                                    if ( options.opt_verbose ) {
                                        cout << "Packet " << setw(width) << i << " : " << msg << " (delta " << t << " μs, " << ms << " ms, " << ts << " s)" << endl;
                                    }
                                    else {
                                        cout << "Packet " << setw(width) << i << " : (delta " << t << " μs, " << ms << " ms, " << ts << " s)" << endl;
                                    }
                                }
                            }
                        }
                        else {
                            cerr << "ERROR: Receiving packet failed." << endl;
                        }
                    }
                    else {
                        cerr << "ERROR: Send packet failed." << endl;
                    }
                }
                else {
                    cerr << "ERROR: Create packet failed." << endl;
                }
                if ( client.sleep( options.opt_delay ) == false ) {
                    cerr << "ERROR: nanosleep() failed." << endl;
                    break;
                }
                i++;
            } while ( i <= options.opt_count );
            if ( options.opt_verbose and (not options.opt_quiet) ) {
                rtt_data.sum_data_points();
                cout << endl
                     << "Data points : " << rtt_data.get_data_size() << endl
                     << "              " << rtt_data.get_ts_size() << endl
                     << "RTT average : " << rtt_data.data_avg << " μs" << endl
                     << "RTT variance: " << rtt_data.data_stdev << " μs" << endl
                     << "RTT data sum: " << rtt_data.data_sum << " μs" << endl << endl;
            }
        }
        else {
            cerr << "ERROR: Client could not be initialised. Reason: " << client.last_error << endl;
        }
        rtt_data.sync_data();
    }

    return(rc);
}
