// =====================================================================================
//
//       Filename:  networking.cc
//
//    Description:  Server and client code. 
//
//        Version:  1.0
//        Created:  11. des. 2022 kl. 02.08 +0100
//       Revision:  none
//       Compiler:  clang
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web-luchs.at/
//
// =====================================================================================

#include <boost/asio.hpp>
#include <boost/range/algorithm/count.hpp>
#include <boost/range/combine.hpp>
#include <boost/tuple/tuple.hpp>
#include <cerrno>
#include <chrono>
#include <execution>
#include <filesystem>
#include <iostream>
#include <limits>
#include <numeric>
#include <string>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termio.h>
#include <unistd.h>
#include <uuid/uuid.h>

#include "networking.h"

using namespace boost;
using namespace std;
namespace fs = std::filesystem;

//----------------------------------------------------------------------
// Globals     
//----------------------------------------------------------------------

// Used for catching signals
extern int signal_received;

//----------------------------------------------------------------------
// Networking helper functions
//----------------------------------------------------------------------

// Get fresh UUID
string create_uuid( void ) {
    uuid_t uuid;
    char uuid_string[37];

    uuid_generate_random( uuid );
    memset( static_cast<void *>(uuid_string), 0, 37 );
    uuid_unparse_lower( uuid, uuid_string );
    string u(uuid_string);
    return(u);
}

// Do a DNS query
string get_address( string &name, unsigned short int port ) {
    asio::io_service ios;

    // Create Boost:Asio query object and a resolver object
    asio::ip::udp::resolver::query resolver_query( name, to_string(port), asio::ip::udp::resolver::query::numeric_service );
    asio::ip::udp::resolver resolver(ios);

    system::error_code ec;
    asio::ip::udp::resolver::iterator it = resolver.resolve(resolver_query, ec);

    // Check for DNS error
    if ( ec != system::errc::success ) {
        return( string("ERROR") );
    }
    else {
        asio::ip::udp::endpoint endpoint = it->endpoint();
        asio::ip::address address = endpoint.address();
        return( address.to_string() );
    }
}

// Return the name of the localhost (code taken from the hostname utility source).
// This is a helper function for get_fqdn(). The function allocates memory which must be
// freed after use (this is done in the get_fqdn() 
char *localhost( void ) {
	char *buf = nullptr;
	size_t buf_len = 0;
	int myerror = 0;

	do {
        errno = 0;
        if ( buf ) {
            buf_len += buf_len;
            buf = static_cast<char *>( realloc(static_cast<void *>(buf), buf_len) );
            if ( buf == nullptr ) return( nullptr );
        } else {
            buf_len = 128; // Initial guess
            buf = static_cast<char *>( malloc(buf_len) );
            if ( buf == nullptr ) return( nullptr );
        }
	} while ( ( (myerror = gethostname(buf, buf_len)) == 0 && !memchr (buf, '\0', buf_len)) || errno == ENAMETOOLONG );

	// gethostname() failed, abort.
	if (myerror) {
        buf = nullptr;
    }

	return buf;
}

// Get the local FQDN
string get_fqdn( void ) {
	struct addrinfo *res;
    struct addrinfo hints;
    char *p;
    int ret;
    string fqdn = string("ERROR"); // This string is an error marker.

    memset( &hints, 0, sizeof(struct addrinfo) );
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags    = AI_CANONNAME;
    p = localhost();
    if ( p != nullptr ) {
        ret = getaddrinfo( p, NULL, &hints, &res );
        free(p);
        if ( ret == 0 ) {
            fqdn = string( res->ai_canonname );
        }
    }

    return(fqdn);
}

// Check if the string is an IPv4 address (by using Boost Asio)
bool is_ipv4_address( string &address ) {
    system::error_code ec;
    bool rc = false;

    asio::ip::address addr_object = asio::ip::address::from_string( address, ec );
    if ( ec == system::errc::success ) {
        rc = addr_object.is_v4();
    }

    return(rc);
}

// Check if the string is an IPv6 address (by using Boost Asio)
bool is_ipv6_address( string &address ) {
    system::error_code ec;
    bool rc = false;

    asio::ip::address addr_object = asio::ip::address::from_string( address, ec );
    if ( ec == system::errc::success ) {
        rc = addr_object.is_v6();
    }

    return(rc);
}

// Sleep for a couple of nanoseconds.
int my_sleep( unsigned short int d ) {
    struct timespec ts;

    unsigned int milliseconds = static_cast<unsigned int>(d);
    ts.tv_sec  = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    return( nanosleep( &ts, NULL ) );
}

//----------------------------------------------------------------------
// SERVER CLASS
//----------------------------------------------------------------------

// --- Constructor -----------------------------------------------------
rtt_server::rtt_server( string address, unsigned int port ) {
    // Set reuse value
    reuse_value = 1;
    // The IP we will use for binding the server
    string ip;

    // Check if we need a DNS resolution
    if ( ( is_ipv4_address( address ) == false ) and ( is_ipv6_address( address ) == false ) ) {
        ip = get_address( address, port );
        if ( ip.compare( string("ERROR") ) == 0 ) {
            last_error = string("get_address() failed. Address = ") + address;
            return;
        }
    }
    else {
        ip = address;
    }

#ifdef DEBUG
    cout << "DEBUG: Address " << address << endl;
    cout << "DEBUG: IP      " << ip << endl;
#endif

    // Check IP and open appropriate socket
    use_ipv4 = is_ipv4_address( ip );
    if ( not use_ipv4 ) {
        use_ipv6 = is_ipv6_address( ip );
        if ( use_ipv6 ) {
            socket_fd = socket( AF_INET6, SOCK_DGRAM, IPPROTO_UDP );
        }
        else {
            is_initialised = false;
            last_error = string("Address is neither IPv4 nor IPv6.");
            return;
        }
    }
    else {
        socket_fd = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
        use_ipv6 = false;
    }

    if ( use_ipv4 ) {
        struct_length = sizeof(server_addr);
#ifdef DEBUG
        cout << "DEBUG: Use IPv4 with " << struct_length << " bytes socket length." << endl;
#endif
    }
    if ( use_ipv6 ) {
        struct_length = sizeof(server6_addr);
#ifdef DEBUG
        cout << "DEBUG: Use IPv6 with " << struct_length << " bytes socket length." << endl;
#endif
    }

    bool is_ip = use_ipv4 or use_ipv6;

    if ( socket_fd == -1 ) {
        is_initialised = false;
        return;
    }
    else {
        if ( use_ipv4 ) {
            memset( static_cast<void *>(&server_addr), 0, struct_length );
            memset( static_cast<void *>(&client_addr), 0, sizeof(client_addr) );
            server_addr.sin_family      = AF_INET;
            server_addr.sin_port        = htons( port );
            int c = inet_pton( AF_INET, ip.c_str(), static_cast<void *>(&server_addr.sin_addr) );
            if ( c == 1 ) {
                setsockopt( socket_fd, SOL_SOCKET, SO_REUSEPORT, &reuse_value, sizeof(reuse_value) );
            }
            else {
                is_initialised = false;
                last_error = string("setsockopt() (IPv4) failed.");
            }
        }
        if ( use_ipv6 ) {
            memset( static_cast<void *>(&server6_addr), 0, struct_length );
            memset( static_cast<void *>(&client6_addr), 0, sizeof(client6_addr) );
            server6_addr.sin6_family      = AF_INET6;
            server6_addr.sin6_port        = htons( port );
            int c = inet_pton( AF_INET6, ip.c_str(), static_cast<void *>(&server6_addr.sin6_addr) );
            if ( c == 1 ) {
                setsockopt( socket_fd, SOL_SOCKET, SO_REUSEPORT, &reuse_value, sizeof(reuse_value) );
            }
            else {
                is_initialised = false;
                last_error = string("setsockopt() (IPv6) failed.");
            }
        }
    }

    // Bind call
    if ( (not is_initialised) and is_ip ) {
        int r = -1;
        if ( use_ipv4 ) {
            r = bind( socket_fd, reinterpret_cast<const struct sockaddr *>(&server_addr), struct_length );
            last_error = string("IPv4 ");
        }
        else if ( use_ipv6 ) {
            r = bind( socket_fd, reinterpret_cast<const struct sockaddr *>(&server6_addr), struct_length );
            last_error = string("IPv6 ");
        }
        // If the bind call succeeds, then the network part can work
        if ( r == 0 ) {
            is_initialised = true;
            packets = 0;
            packet_limit = numeric_limits<unsigned int>::max();
            memset( static_cast<void *>(&recv_buffer), 0, buffer_size );
            reply_buffer.clear();
            last_error.clear();
        }
        else {
            is_initialised = false;
            last_error = last_error + string("bind() call failed - ") + strerror(errno);
        }
    }
    else {
        last_error = string("Missing initialisation or address is not an IP.");
    }
}

// --- Destructor ------------------------------------------------------
rtt_server::~rtt_server() {
    if ( socket_fd != -1 ) {
        close ( socket_fd );
    }
}

// --- Create data for return packet -----------------------------------
// - Create UID
// - Take the current time
// - Determine the Fully Qualified Domain Name (FQDN) of the host
//
bool rtt_server::create_reply_packet( void ) {
    bool rc = true;
    uint64_t microseconds_since_epoch = chrono::duration_cast<chrono::microseconds>(chrono::system_clock::now().time_since_epoch()).count();
    string hostname_s = get_fqdn();

    if ( current_uuid.size() > 0 ) {
        reply_buffer  = current_uuid + string("|") + to_string(microseconds_since_epoch) + string("|") + hostname_s;
    }

    return(rc);
}

// --- Main event loop -------------------------------------------------
// 
// GCC v10 reports error: ‘recv_length’ may be used uninitialized in this function [-Werror=maybe-uninitialized]
// This error is wrong, because GCC doesn't see the memset() call. . Clang doesn't report it. So we turn off this warning
// only for GCC and only for this function.
#ifndef __clang__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
void rtt_server::event_loop( void ) {
    ssize_t recv_length = 0;

    // This is an endless loop. Abort condition are singals and a counter condition.
    do {
        memset( static_cast<void *>(&recv_buffer), 0, buffer_size );
        if ( use_ipv4 ) {
            recv_length = recvfrom( socket_fd,
                                    static_cast<void *>(&recv_buffer),
                                    buffer_size,
                                    0,
                                    reinterpret_cast<struct sockaddr *>(&client_addr),
                                    &struct_length
                                  );
        }
        if ( use_ipv6 ) {
            recv_length = recvfrom( socket_fd,
                                    static_cast<void *>(&recv_buffer),
                                    buffer_size,
                                    0,
                                    reinterpret_cast<struct sockaddr *>(&client6_addr),
                                    &struct_length
                                  );
        }
        if ( recv_length == -1 ) {
            // In case of error, put server to a short sleep and ignore error condition.
            usleep(999999);
        }
        else {
            // Handle packet
            if ( recv_length > 0 ) {
#ifdef DEBUG
                cout << "DEBUG: Received packet with " << recv_length << " bytes." << endl;
#endif
                // Parse UUID
                string data( recv_buffer, recv_length );
                auto start = 0;
                auto end   = data.find("|");
                if ( end != string::npos ) {
                    current_uuid = data.substr( start, end );
                }
                // Create return packet
                if ( create_reply_packet() ) {
                    if ( send_reply() ) {
#ifdef DEBUG
                        cout << "DEBUG: Sent reply packet. UUID: " << current_uuid << endl;
#endif
                        current_uuid.clear();
                        reply_buffer.clear();
                    }
#ifdef DEBUG
                    else {
                        cout << "DEBUG: Error sending return packet." << endl;
                    }
#endif
                }
                // Reset status code for next packet run
                recv_length = -1;
            }
            packets++;
        }
        if ( signal_received > 0 ) {
            packets = packet_limit;
            break;
        }
    } while ( packets < packet_limit );

    return;
}
#ifndef __clang__
#pragma GCC diagnostic pop
#endif

// --- Send reply packet -----------------------------------------------
bool rtt_server::send_reply( void ) {
    bool rc = false;
    ssize_t bytes = -1;

    if ( not reply_buffer.empty() ) {
        if ( use_ipv4 ) {
            bytes = sendto( socket_fd, reply_buffer.c_str(), reply_buffer.length(), 0, reinterpret_cast<struct sockaddr *>(&client_addr), struct_length );
        }
        if ( use_ipv6 ) {
            bytes = sendto( socket_fd, reply_buffer.c_str(), reply_buffer.length(), 0, reinterpret_cast<struct sockaddr *>(&client6_addr), struct_length );
        }
        if ( bytes > 0 ) {
            rc = true;
        }
    }

    return(rc);
}

// --- Signal handler to abort the event loop --------------------------
// 
// Signal sets number of packets to packet limit. Server will exit after the first packet is received
// after the signal was caught (due to recvfrom() blocking).
void rtt_server::signal_handler( int signal ) {
    packets = packet_limit;
    return;
}

//----------------------------------------------------------------------
// CLIENT CLASS
//----------------------------------------------------------------------

// --- Constructor -----------------------------------------------------
rtt_client::rtt_client( string address, unsigned int port ) {
    // The IP we will use for binding the server
    string ip;

    // Check if we need a DNS resolution
    if ( ( is_ipv4_address( address ) == false ) and ( is_ipv6_address( address ) == false ) ) {
        ip = get_address( address, port );
        if ( ip.compare( string("ERROR") ) == 0 ) {
            last_error = string("get_address() failed. Address = ") + address;
            is_initialised = false;
            return;
        }
    }
    else {
        ip = address;
    }

#ifdef DEBUG
    cout << "DEBUG: Address " << address << endl;
    cout << "DEBUG: IP      " << ip << endl;
#endif

    // Check IP and open appropriate socket
    use_ipv4 = is_ipv4_address( ip );
    if ( use_ipv4 ) {
        socket_fd = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
        use_ipv6 = false;
    }
    else {
        use_ipv6 = is_ipv6_address( ip );
        if ( use_ipv6 ) {
            socket_fd = socket( AF_INET6, SOCK_DGRAM, IPPROTO_UDP );
        }
        else {
            is_initialised = false;
        }
    }

    // Set structure length
    if ( use_ipv4 ) struct_length = sizeof(server_addr);
    if ( use_ipv6 ) struct_length = sizeof(server6_addr);

    // Make socket ready for sending packet
    if ( socket_fd == -1 ) {
        is_initialised = false;
    }
    else {
        if ( use_ipv4 ) {
            memset( static_cast<void *>(&server_addr), 0, sizeof(server_addr) );
            server_addr.sin_family      = AF_INET;
            server_addr.sin_port        = htons( port );
            int c = inet_pton( AF_INET, ip.c_str(), static_cast<void *>(&server_addr.sin_addr) );
            if ( c == 1 ) {
                is_initialised = true;
            }
            else {
                is_initialised = false;
            }
        }
        if ( use_ipv6 ) {
            memset( static_cast<void *>(&server6_addr), 0, sizeof(server6_addr) );
            server6_addr.sin6_family      = AF_INET6;
            server6_addr.sin6_port        = htons( port );
            int c = inet_pton( AF_INET6, ip.c_str(), static_cast<void *>(&server6_addr.sin6_addr) );
            if ( c == 1 ) {
                is_initialised = true;
                memset( static_cast<void *>(&recv_buffer), 0, sizeof(recv_buffer) );
            }
            else {
                is_initialised = false;
            }
        }
    }

    packet_received = false;
}

// --- Destructor ------------------------------------------------------
rtt_client::~rtt_client() {
    if ( socket_fd != -1 ) {
        close ( socket_fd );
    }
}

// --- Create data for packet ------------------------------------------
bool rtt_client::create_ping_packet( void ) {
    bool rc = true;
    uint64_t microseconds_since_epoch = chrono::duration_cast<chrono::microseconds>(chrono::system_clock::now().time_since_epoch()).count();
    send_timestamp = microseconds_since_epoch;
    string hostname_s = get_fqdn();

    current_uuid = create_uuid();
    send_buffer  = current_uuid + string("|") + to_string(microseconds_since_epoch) + string("|") + hostname_s;

    return(rc);
}

// --- Get delta value of timestamps (numeric, nanoseconds ) -----------
uint64_t rtt_client::get_delta_nano( void ) {
    uint64_t r = 0;

    if ( ts_delta > 0 ) {
        r = ts_delta;
    }
    return(r);
}

// --- Get delta value of timestamps (numeric, milliseconds ) ----------
double rtt_client::get_delta_micro( void ) {
    double ts = 0.0;

    if ( ts_delta > 0 ) {
        ts = static_cast<double>(ts_delta) / 1000.0;
    }
    return(ts);
}

// --- Get delta value of timestamps (numeric, milliseconds ) ----------
double rtt_client::get_delta_milli( void ) {
    double ts = 0.0;

    if ( ts_delta > 0 ) {
        ts = static_cast<double>(ts_delta) / 1000000.0;
    }
    return(ts);
}

// --- Get delta value of timestamps (numeric, seconds ) ---------------
double rtt_client::get_delta_seconds( void ) {
    double ts = 0.0;

    if ( ts_delta > 0 ) {
        ts = static_cast<double>(ts_delta) / 1000000000.0;
    }
    return(ts);
}

// --- Get delta value of timestamps (string in milliseconds) ----------
string rtt_client::get_delta_s( void ) {
    string d;

    uint64_t pre  = ts_delta / 1000;
    uint64_t post = ts_delta % 1000;
    d = to_string(pre) + string(".") + to_string(post);
    return(d);
}

// --- Get received string data from server ----------------------------
string rtt_client::get_packet_data_as_string( void ) {
    string s;

    if ( packet_received ) {
        s = recv_uuid + string(" ") + recv_timestamp_s;
        packet_received = false;
    }

    return(s);
}

// --- Receive packet from server --------------------------------------
bool rtt_client::recv_packet( void ) {
    bool rc = false;
    ssize_t bytes = -1;

    memset( static_cast<void *>(recv_buffer), 0, buffer_size );
    ts_delta = 0;

    if ( use_ipv4 ) {
        bytes = recvfrom( socket_fd, recv_buffer, buffer_size, 0, reinterpret_cast<struct sockaddr *>(&server_addr), &struct_length );
    }
    if ( use_ipv6 ) {
        bytes = recvfrom( socket_fd, recv_buffer, buffer_size, 0, reinterpret_cast<struct sockaddr *>(&server6_addr), &struct_length );
    }

    // Extract information from the packet
    if ( bytes > 0 ) {
#ifdef DEBUG
        cout << "DEBUG: Received " << bytes << " from server." << endl;
#endif
        packet_received = true;
        string data( recv_buffer, bytes );
        if ( boost::count( data, '|' ) == 2 ) {
            auto start = 0;
            auto end   = data.find("|");
            auto last  = data.rfind("|");
            if ( (end != string::npos) and (last != string::npos) ) {
                recv_uuid        = data.substr( start, end );
                recv_timestamp_s = data.substr( end+1, last-end-1 );
                recv_host        = data.substr( last+1 );
                rc = true;
                try {
                    recv_timestamp = stoul( recv_timestamp_s );
                    if ( recv_timestamp > send_timestamp ) {
                        ts_delta = recv_timestamp - send_timestamp;
                    }
                    if ( recv_timestamp < send_timestamp ) {
                        ts_delta = send_timestamp - recv_timestamp;
                    }
                }
                catch (...) {
                    recv_timestamp = 0;
                    ts_delta       = 0;
                    rc = false;
                }
            }
        }
    }

    return(rc);
}

// --- Send packet -----------------------------------------------------
bool rtt_client::send_packet( void ) {
    bool rc = false;
    ssize_t bytes = -1;

    if ( not send_buffer.empty() ) {
        if ( use_ipv4 ) {
            bytes = sendto( socket_fd, send_buffer.c_str(), send_buffer.length(), 0, reinterpret_cast<struct sockaddr *>(&server_addr), struct_length );
        }
        if ( use_ipv6 ) {
            bytes = sendto( socket_fd, send_buffer.c_str(), send_buffer.length(), 0, reinterpret_cast<struct sockaddr *>(&server6_addr), struct_length );
        }
        if ( bytes > 0 ) {
#ifdef DEBUG
            cout << "DEBUG: Sent " << bytes << " bytes." << endl;
#endif
            rc = true;
        }
    }

    return(rc);
}

// --- Sleep for d milliseconds ----------------------------------------
bool rtt_client::sleep( unsigned short int d ) {
    if ( my_sleep(d) == 0 ) {
        return(true);
    }
    else {
        return(false);
    }
}

//----------------------------------------------------------------------
// RTT DATA CLASS
//----------------------------------------------------------------------

// --- Constructors ----------------------------------------------------
delta_ts::delta_ts( void ) {
    // Set defaults
    init_state();
    // Clear save checkpoint, because we have no file open.
    save_checkpoint = 0;
    return;
}

delta_ts::delta_ts( string store ) {
    init_state();
    data_file_name = store;
    is_initialised = open_data_file();
    if ( is_initialised ) {
        save_checkpoint = 2000;
    }
    return;
}

// --- Destructor ------------------------------------------------------
delta_ts::~delta_ts( void ) {
    if ( ( data.size() > 0 ) and ( ts.size() > 0 ) ) {
        if ( not write_data_points() ) {
            cerr << "ERROR: Could not write remaiing data points." << endl;
        }
    }
    close_data_file();
    return;
}

// --- Add data point --------------------------------------------------
bool delta_ts::add_data_point( uint64_t d ) {
    bool rc = false;
    uint64_t t;

    try {
        t = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
        data.emplace_back( d );
        ts.emplace_back( t );
        if ( d > max_d ) max_d = d;
        if ( d < min_d ) min_d = d;
        if ( data_points < numeric_limits<unsigned int>::max() ) {
            data_points++;
            if ( data_points > save_checkpoint ) {
                rc = write_data_points();
            }
        }
    }
    catch (...) {
        cerr << "ERROR: Could not add data point." << endl;
        rc = false;
    }

    return(rc);
}

// --- Close data file -------------------------------------------------
bool delta_ts::close_data_file( void ) {
    bool rc = false;

    try {
        if ( data_file.is_open() ) {
            data_file.close();
        }
    }
    catch (...) {
        // rc is already set to false.
        cerr << "ERROR: Could not close data log file." << endl;
    }

    return(rc);
}

// --- Get number of data points ---------------------------------------
unsigned int delta_ts::get_data_size( void ) {
    return( data.size() );
}

// --- Get number of ts data points ------------------------------------
unsigned int delta_ts::get_ts_size( void ) {
    return( ts.size() );
}

// --- Initialise internal data ----------------------------------------
void delta_ts::init_state( void ) {
    data.clear();
    ts.clear();
    data_points = 0;
    data_sum = 0;
    max_d = 0;
    min_d = numeric_limits<uint64_t>::max();
    save_checkpoint = 1000;

    return;
}

// --- Open data file for writing --------------------------------------
bool delta_ts::open_data_file( void ) {
    fs::path data_log( data_file_name );

    try {
        if ( fs::exists( data_log ) ) {
            data_file.open( data_file_name, ofstream::app );
        }
        else {
            data_file.open( data_file_name, ofstream::out | ofstream::trunc );
        }
        return(true);
    }
    catch (...) {
        cerr << "ERROR: Could not open data log file." << endl;
        return(false);
    }
}

// --- Set data file name ----------------------------------------------
bool delta_ts::set_data_file_name( string &store ) {
    bool rc = false;

    if ( store.empty() ) {
        cerr << "ERROR: Data lot path/filename is empty." << endl;
    }
    else {
        try {
            // Check if path and filename are valid
            fs::path data_log( store );
            data_file_name = data_log.string();
            rc = open_data_file();
        }
        catch (...) {
            cerr << "ERROR: Invalid path or filename (" << store << ")" << endl;
            return(false);
        }
    }

    return(rc);
}

// --- Set save checkpoint number --------------------------------------
void delta_ts::set_save_checkpoint( unsigned int c ) {
    save_checkpoint = c;
    if ( c == 0 ) {
        close_data_file();
    }
    return;
}

// --- Sum all stored data points --------------------------------------
bool delta_ts::sum_data_points( void ) {
    bool rc = false;
    double a;

    if ( data.size() > 0 ) {
#ifndef STATIC_CODE
        data_sum = reduce( std::execution::par, data.begin(), data.end() );
#else
        data_sum = accumulate( data.begin(), data.end(), 0 );
#endif
        data_avg = data_sum / data.size();
        // Calculate standard deviation
        for_each( data.begin(), data.end(), [&](const double d) {
            a += (d - data_avg) * (d - data_avg);
        });
        data_stdev = sqrt(a / (static_cast<double>(data.size()-1)));
    }
    else {
        data_avg = data_sum = 0;
    }

    return(rc);
}

// --- Write all stored data -------------------------------------------
bool delta_ts::sync_data( void ) {
    return( write_data_points() );
}

// --- Write data point if limit is exceeded ---------------------------
//
// We need to iterate through two vectors at once. This is done by using
// Boost::combine. See https://stackoverflow.com/questions/8511035/sequence-zip-function-for-c11
// for more information

bool delta_ts::write_data_points( void ) {
    bool rc = false;
    uint64_t d,t;

    if ( save_checkpoint != 0 ) {
        try {
            if ( data_file.is_open() ) {
                for( auto tup : boost::combine( ts, data ) ) {
                    boost::tie( t, d ) = tup;
                    data_file << t << "," << d << endl;
                }
                data.clear();
                ts.clear();
                rc = true;
            }
        }
        catch (...) {
            cerr << "ERROR: Could not write to data log file." << endl;
            rc = false;
        }
    }
    else {
        rc = true;
    }

    return(rc);
}
