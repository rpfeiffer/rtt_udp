#!/bin/bash

CXXFLAGS="-Wall -Werror -I. -O2 -march=native -std=c++20 -flto -DSTATIC_CODE"
SECFLAGS="-fPIE -fstack-protector -fsanitize=signed-integer-overflow -fsanitize=unsigned-integer-overflow -fstack-clash-protection -fsanitize=undefined -fsanitize-minimal-runtime -Wformat -Wformat-security -Werror=format-security -Wnull-pointer-arithmetic -D_FORTIFY_SOURCE=2"
LDFLAGS="/usr/lib/x86_64-linux-gnu/libboost_program_options.a /usr/lib/x86_64-linux-gnu/libboost_system.a /usr/lib/x86_64-linux-gnu/libpthread.a /usr/lib/x86_64-linux-gnu/libuuid.a"

CXX=clang++-15

$CXX $CXXFLAGS $SECFLAGS configuration.cc networking.cc udprtt.cc $LDFLAGS --static -o udprtt_static
