# Bugs / To-Dos

This code has a few bugs, and it is missing parts of the implementation.

- Command line options and config file options are a bit confusing.
- Compiler version is hardcoded into the build files.
- Compiling a static version is not possible, because the TBB library doesn't support it.
- Encryption of the UDP packets is not implemented.
- Manual is missing.
- Stopping the server only works after it has received a packet after receiving an abort signal.
- Time measurement is not accurate.
