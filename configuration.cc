// =====================================================================================
//
//       Filename:  configuration.cc
//
//    Description:  Collection of command line and configuration file options.
//
//        Version:  1.0
//        Created:  11. dec. 2022 kl. 22.58 +0100
//       Revision:  none
//       Compiler:  clang
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web-luchs.at/
//
// =====================================================================================

#include <boost/config.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <string>

#include <sysexits.h>

#include "configuration.h"

using namespace std;
using namespace boost;
namespace popt = boost::program_options;

//----------------------------------------------------------------------
// Constructor function
//----------------------------------------------------------------------
config_options::config_options() {
    opt_config = string("/etc/udprtt.conf");
    opt_count = conf_count = 9;
    opt_debug = 0;
    opt_delay = conf_delay = 500;
    opt_port = conf_port = 4545;
    opt_quiet = false;
    opt_quiet_int = 1;
    conf_data_file = string("/var/tmp/udprtt.log");
}

//----------------------------------------------------------------------
// Deconstructor function
//----------------------------------------------------------------------
config_options::~config_options() {
}

//----------------------------------------------------------------------
// Helper function
//----------------------------------------------------------------------

// Check if file is readable
bool config_options::is_file_accessible( string &filename ) {
    bool accessible = true;
    ifstream file_check;

    // Never, never, never use only stat() or functions relying on stat() in order to check access permissions!
    try {
        file_check.open( filename, ifstream::in );
        file_check.close();
    }
    catch ( std::ifstream::failure &e ) {
        accessible = false;
        if ( not opt_quiet ) {
            cerr << "ERROR: Error reading file " << filename << " (" << e.what() << ")" << endl;
        }
    }
    catch (...) {
        accessible = false;
        if ( not opt_quiet ) {
            cerr << "ERROR: Unspecified error in is_file_accessible() function." << endl;
        }
    }
    return(accessible);
}

//----------------------------------------------------------------------
// Parse command line options and configuration file
//----------------------------------------------------------------------

int config_options::parse_configuration( int argc, char **argv ) {
    // Return code
    int rc = EX_OK;
    // Build time
    const string built_timestamp(__TIMESTAMP__);
    // Command line arguments and config file options
    popt::variables_map vm,vm_cfg;
    bool cli_count_present = false;

    // --- Parse command line ------------------------------------------
    try {
        // Define possible command line options
        popt::options_description desc("Command line options for udprtt tool");
        desc.add_options()
            ("client,C", "Run as client.")
            ("config,c", popt::value<string>(&opt_config), "Configuration file to be used.")
            ("count", popt::value<unsigned int>(&opt_count)->default_value(9), "How many packets to send.")
            ("data_file", popt::value<string>(&opt_data_file), "File to store the RTT data to.")
            ("debug,d", popt::value<unsigned short int>(&opt_debug)->default_value(0), "Enable debug mode.")
            ("delay,D", popt::value<unsigned short int>(&opt_delay)->default_value(500), "Delay between packets in milliseconds.")
            ("help,h", "Show a short help message with explanations (hopefully).")
            ("port,p", popt::value<unsigned short int>(&opt_port)->default_value(4545), "Port to use.")
            ("quiet,q", popt::value<unsigned short int>(&opt_quiet_int)->default_value(0), "Be quiet about the whole operation.")
            ("server,S", "Run as server.")
            ("target,t", popt::value<string>(&opt_target), "Target server address.")
            ("verbose,v", "Enable verbose output messages.")
        ;

        // Parse command line and extract options
        popt::store( popt::parse_command_line(argc, argv, desc), vm );
        popt::notify( vm );

        // Validate command line otions
        if ( (vm.count("help") > 0) or (vm.count("h") > 0) ) {
            cout << programme_name << " - Version V" << version_major << "." << version_minor << " (built " << built_timestamp << ")" << endl << endl;
            cout << desc << endl;
            help_was_called = true;
            return(rc);
        }
        else {
            help_was_called = false;
        }

        if ( (vm.count("verbose") > 0) or (vm.count("v") > 0) ) {
            opt_verbose = true;
        }
        else {
            opt_verbose = false;
        }

        if ( (vm.count("config") > 0) or (vm.count("c") > 0) ) {
            opt_config = vm["config"].as<string>();
        }

        if ( vm.count("count") > 0 ) {
            opt_count  = vm["count"].as<unsigned int>();
            cli_count_present = true;
        }

        if ( (vm.count("client") > 0) or (vm.count("C") > 0) ) {
            is_client = true;
            is_server = false;
        }
        else {
            is_client = false;
            is_server = true;
        }

        if ( vm.count("data_file") > 0 ) {
            opt_data_file  = vm["data_file"].as<string>();
            conf_data_file = opt_data_file;
        }

        if ( (vm.count("delay") > 0) or (vm.count("D") > 0) ) {
            opt_delay  = vm["delay"].as<unsigned short int>();
            conf_delay = opt_delay;
        }

        if ( (vm.count("debug") > 0) or (vm.count("d") > 0) ) {
            opt_debug = vm["debug"].as<unsigned short int>();
        }

        if ( (vm.count("port") > 0) or (vm.count("p") > 0) ) {
            opt_port = vm["port"].as<unsigned short int>();
        }

        if ( (vm.count("quiet") > 0) or (vm.count("q") > 0) ) {
            opt_quiet_int = vm["quiet"].as<unsigned short int>();
            if ( opt_quiet_int > 0 ) {
                opt_quiet = true;
            }
            else {
                opt_quiet = false;
            }
        }

        if ( (vm.count("server") > 0) or (vm.count("S") > 0) ) {
            is_client = false;
            is_server = true;
        }
        else {
            is_client = true;
            is_server = false;
        }
    }
    catch (const char* eh) {
        if ( not opt_quiet ) {
            cerr << eh << endl;
        }
        rc = EX_CONFIG;
    }

    // --- Parse configuration file -----------------------------------
    // 
    // The configuration file is optional, so if it is not readable, then
    // the code will skip it.
    if ( is_file_accessible(opt_config) ) {
        try {
            // Define possible configuration file options
            popt::options_description config_file_options("Configuration file options");
            config_file_options.add_options()
                ("address", popt::value<string>(&conf_address), "Address to use. Can also be a name.")
                ("checkpoint", popt::value<unsigned int>(&conf_checkpoint)->default_value(2000), "Number of data points to keep in memory.")
                ("count", popt::value<unsigned int>(&conf_count)->default_value(9), "How many packets to send.")
                ("data_file", popt::value<string>(&conf_data_file), "File to store the RTT data to.")
                ("delay", popt::value<unsigned short int>(&conf_delay)->default_value(500), "Delay between pacets in milliseconds.")
                ("key", popt::value<string>(&conf_key), "Encryption key.")
                ("port", popt::value<unsigned short int>(&conf_port)->default_value(4545), "Port to use.")
            ;

            // Read configuration file
            ifstream config_file( opt_config.c_str(), std::ifstream::in );
            if ( config_file.is_open() ) {
                popt::store( popt::parse_config_file( config_file, config_file_options ), vm_cfg );
                popt::notify( vm_cfg );
                config_file.close();

                if ( vm_cfg.count("address") > 0 ) {
                    conf_address = vm_cfg["address"].as<string>();
                }

                if ( vm_cfg.count("checkpoint") > 0 ) {
                    conf_checkpoint = vm_cfg["checkpoint"].as<unsigned int>();
                    conf_checkpoint_set = true;
                }
                else {
                    conf_checkpoint_set = false;
                }

                if ( vm_cfg.count("count") > 0 ) {
                    // Command line value is preferred
                    if ( cli_count_present ) {
                        conf_count = opt_count;
                    }
                    else {
                        conf_count = vm_cfg["count"].as<unsigned int>();
                        opt_count  = conf_count;
                    }
                }

                if ( vm_cfg.count("data_file") > 0 ) {
                    conf_data_file = vm_cfg["data_file"].as<string>();
                    opt_data_file  = conf_data_file;
                }

                if ( vm_cfg.count("delay") > 0 ) {
                    conf_delay = vm_cfg["delay"].as<unsigned short int>();
                    opt_delay  = conf_delay;
                }

                if ( vm_cfg.count("key") > 0 ) {
                    conf_key = vm_cfg["key"].as<string>();
                }

                if ( vm_cfg.count("port") > 0 ) {
                    conf_port = vm_cfg["port"].as<unsigned short int>();
                }
            }
        }
        catch (const char* eh) {
            if ( not opt_quiet ) {
                cerr << eh << endl;
            }
            rc = EX_CONFIG;
        }
    }

    return(rc);
}
