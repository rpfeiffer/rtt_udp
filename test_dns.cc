#include <boost/asio.hpp>
#include <iostream>

using namespace boost;
using namespace std;

int main( void ) {
    string name = string("web.luchs.at");
    unsigned short port = 4545;
    asio::io_service ios;

    // Create Boost:Asio query object and a resolver object
    asio::ip::udp::resolver::query resolver_query( name, to_string(port), asio::ip::udp::resolver::query::numeric_service );
    asio::ip::udp::resolver resolver(ios);

    system::error_code ec;
    asio::ip::udp::resolver::iterator it = resolver.resolve(resolver_query, ec);

    // Check for DNS error
    if ( ec != system::errc::success ) {
        cout << string("ERROR") << endl;
    }
    else {
        asio::ip::udp::endpoint endpoint = it->endpoint();
        asio::ip::address address = endpoint.address();
        cout << "Found: " << address.to_string() << endl;
    }
}
