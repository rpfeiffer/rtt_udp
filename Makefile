# Makefile for GCC
#
# (C) 2022 by René Pfeiffer <pfeiffer@luchs.at>

CC = gcc
CXX = g++
LD = g++

CFLAGS = -Wall -Werror -I. -O2 -march=native
CXXFLAGS = -Wall -Werror -I. -O2 -march=native -std=c++20
LDFLAGS = -ltbb -luuid -lpthread -lboost_system -lboost_program_options

CXXDEPS = configuration.cc networking.cc udprtt.cc configuration.h networking.h globals.h
OBJS = configuration.o networking.o udprtt.o

%.o: %.cc $(CXXDEPS)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

udprtt: $(OBJS)
	$(LD) -o $@ $^ $(LDFLAGS)

all: udprtt
